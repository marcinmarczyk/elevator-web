import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { ElevatorComponent } from './elevator/elevator.component';
import { PanelComponent } from './panel/panel.component';

@NgModule({
  declarations: [
    ElevatorComponent,
    PanelComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [ElevatorComponent,PanelComponent]
})
export class AppModule { }

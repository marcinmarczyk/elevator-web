import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

import { Options } from './options';
import { ElevatorsDto } from './elevators-dto';
import { environment } from 'src/environments/environment';
import { CurrentRequestsDto } from './current-requests-dto';

@Injectable({
  providedIn: 'root'
})
export class ElevatorService {

  private baseUrl = environment.baseUrl;

  constructor(
    private http: HttpClient
  ) { }

  getOptions(): Observable<Options> {
    const GET_OPTIONS_URL = `${this.baseUrl}/options`;

    return this.http.get<Options>(GET_OPTIONS_URL)
      .pipe(
        catchError(this.handleError<Options>('getOptions', null))
      );
  }

  getElevators(options: Options): Observable<ElevatorsDto> {
    const GET_ELEVATORS_URL = `${this.baseUrl}/currentElevators`;
    
    return this.http.get<ElevatorsDto>(GET_ELEVATORS_URL)
      .pipe(
        catchError(this.handleError<ElevatorsDto>('getElevators', null))
      );
  }

  requestElevator(floor: number): void {
    const REQUEST_ELEVATOR_URL = `${this.baseUrl}/requestElevator/${floor}`;

    this.http.get(REQUEST_ELEVATOR_URL)
      .pipe(
        catchError(this.handleError('requestElevator', null))
      ).subscribe();
  }

  getCurrentRequests(): Observable<CurrentRequestsDto> {
    const GET_ELEVATORS_URL = `${this.baseUrl}/currentRequests`;
    
    return this.http.get<CurrentRequestsDto>(GET_ELEVATORS_URL)
      .pipe(
        catchError(this.handleError<CurrentRequestsDto>('getCurrentRequests', null))
      );
  }

  chooseDestinationFloor(elevatorId: number, floor: number): void {
    const CHOOSE_DESTINATION_FLOOR_URL = `${this.baseUrl}/chooseFloor/${elevatorId}/${floor}`;

    this.http.get(CHOOSE_DESTINATION_FLOOR_URL)
      .pipe(
        catchError(this.handleError('chooseDestinationFloor', null))
      ).subscribe();
  }

  private handleError<T>(operation = 'operation', result?: T){
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    }
  }
}

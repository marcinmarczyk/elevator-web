import { Component, OnInit } from '@angular/core';

import { ElevatorService } from '../elevator.service';
import { Options } from '../options';
import { Elevator } from '../elevator';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.styl']
})
export class PanelComponent implements OnInit {
  private interval;
  options: Options = new Options();
  elevators: Elevator[] = [];

  constructor(private elevatorService: ElevatorService) { }

  ngOnInit() {
    this.getOptions();
    this.getElevators();
    this.interval = setInterval( () => {
      this.getElevators();
    }, 1000);
  }

  getOptions(): void {
    this.elevatorService.getOptions()
      .subscribe(options => {
        this.options = options;
        this.options.elevatorsArray = Array(this.options.numberOfElevators).fill(0).map((x,i) => i);
        this.options.floorsArray = Array(this.options.numberOfFloors).fill(0).map((x,i) => i).reverse();
      });
  }

  getElevators(): void {
    this.elevatorService.getElevators(this.options)
      .subscribe( elevatorsDto => {
        elevatorsDto.elevatorDtos.forEach(element => {
            this.elevators[element.id] = element;
          });
      });
  }

  getElevatorById(id: number): Elevator {
    return this.elevators.filter(e=> e.id == id)[0] || new Elevator();
  }

  chooseDestinationFloor(elevatorId: number, floor: number): void {
    this.elevatorService.chooseDestinationFloor(elevatorId, floor);
  }

  shouldBeDisabled(elevatorId: number): boolean {
    var e = this.elevators.filter(e => e.id == elevatorId)[0];
    return e.state == 'MOVING';
  }
}

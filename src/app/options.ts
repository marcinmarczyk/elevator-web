export class Options {
    numberOfElevators: number;
    numberOfFloors: number;
    elevatorsArray: number[];
    floorsArray: number[];
}

## Description of the implementation

The UI is build on top of _Angular Framework_.
At the start the application requests options from the API.
Options consist of parameters representing number of elevators and floors.
Based on that data UI is being built.

## How to use the application
Grey rectangles represent the elevators.
Buttons to the right (with dots) represent buttons on each floor, They are used to call the elevator.
Bottom part contains elevator panels, these represent buttons which can be found in each elevator and are used to choose destination floor by user. Panel also contain direction and state of the elevator.

## What to implement next

 * tests

## Build And Run

Project can be build and run using below commands (node.js and npm must be installed):
   
    npm install --silent
    ng serve --open
Default web browser should open application automatically.

Project can be run using Docker and docker-compose
    
    docker-compose up

import { Component, OnInit } from '@angular/core';

import { ElevatorService } from '../elevator.service';
import { Options } from '../options';
import { Elevator } from '../elevator';
import { ElevatorsDto } from '../elevators-dto';

@Component({
  selector: 'app-elevator',
  templateUrl: './elevator.component.html',
  styleUrls: ['./elevator.component.styl']
})
export class ElevatorComponent implements OnInit {

  private interval;
  elevators: Elevator[] = [];
  elevatorsDto: ElevatorsDto;
  options: Options;
  currentRequests: number[] = [];

  constructor(private elevatorService: ElevatorService) { }

  getOptions(): void {
    this.elevatorService.getOptions()
      .subscribe(options => {
        this.options = options;
        this.options.elevatorsArray = Array(this.options.numberOfElevators).fill(0).map((x,i) => i);
        this.options.floorsArray = Array(this.options.numberOfFloors).fill(0).map((x,i) => i).reverse();
      });
  }

  getElevators(): void {
    this.elevatorService.getElevators(this.options)
      .subscribe( elevatorsDto => {
        elevatorsDto.elevatorDtos.forEach(element => {
            this.elevators[element.id] = element;
          });
      });
  }

  getCurrentRequests(): void {
    this.elevatorService.getCurrentRequests()
      .subscribe( result => {
        this.currentRequests = result.currentRequests;
      });
  }

  isWaiting(floor: number, elevator: number): boolean {
    if (this.elevators == undefined) {
      return false;
    }
    var filtered = this.elevators.filter((e) => e.id == elevator);
    if (filtered.length == 0){
      return false;
    }
    return filtered[0].floor == floor && !filtered[0].isMoving;
  }

  isInQueue(floor: number): boolean {
    var filtered = this.currentRequests.filter((f) => f == floor);
    return filtered.length > 0;
  }

  requestElevator(floor: number): void {
    this.elevatorService.requestElevator(floor);
  }

  ngOnInit() {
    this.options = new Options();
    this.options.elevatorsArray = [];
    this.getOptions();
    this.interval = setInterval( () => {
      this.getElevators();
      this.getCurrentRequests();
    }, 1000);
  }

}

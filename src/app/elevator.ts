export class Elevator {
    id: number;
    floor: number;
    isMoving: boolean;
    state: String;
    direction: String;
}
